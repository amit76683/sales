module.exports = {
    sections: {
        oneforce_home: {
            selector: 'body',
            elements: {
                login_id: {locateStrategy: 'xpath', selector: "//input[@id='username']"},
                password: {locateStrategy: 'xpath', selector: "//input[@id='password']"},
                login: {locateStrategy: 'xpath', selector: "//input[@id='Login']"},
                test: {locateStrategy: 'xpath', selector: "(//tr//td//select//option)[1]"},
                user_menu: {locateStrategy: 'xpath', selector: "//div//span[@id='userNavLabel']"},
                log_out: {locateStrategy: 'xpath', selector: "//a[@id='app_logout']"},
                error_message: {locateStrategy: 'xpath', selector: "//div[@id='error']"},
                error_message1: {locateStrategy: 'xpath', selector: "//span[contains(.,'URL No Longer Exists')]"},
            }
        }

    }
};