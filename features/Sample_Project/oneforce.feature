Feature: This is a smoke scenarios for salesforce

  @oneforce @basic1
  Scenario: Verifing Login/Logout Functionality
    Given User opens the salesforce url
    When user enters username and password
    And  user clicks on logout

  @oneforce @basic2
  Scenario: Verifing Login using wrong credentials
    Given User opens the salesforce url
    When user enters username and password1
    And  user got error message

  @oneforce @basic3
  Scenario: To verify HTTP strict Transfer security for oneforce url
    Given User opens the salesforce url with http
    When user enters username and password
    And  user clicks on logout

  @oneforce @bsaic4 @ClickJacking
  Scenario: Click Jacking
    Given User identifies AFBO url and uses it in a code snippet
    Then  User opens the code snippet from the desktop

  @oneforce @basic5 @SQL_Injection
  Scenario: To verify application does not respond when sql injection happens.
    Given   User opens the salesforce url with sql query
    Then    Verify that page does not get load




