var data = require('../../TestResources/oneforce');
var Objects = require(__dirname + '/../../repository/oneforce.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');
var oldWindow,newWindow,oldWindow1,newWindow1
function initializePageObjects(browser, callback) {
    var BPPage = browser.page.oneforce();
    oneforce_home = BPPage.section.oneforce_home;
    callback();
}
//1
            module.exports = function() {
            this.Given(/^User opens the salesforce url$/,function(){
                var URL;
                browser = this;
                var execEnv = data["TestingEnvironment"];
                if (execEnv.toUpperCase() == "QA"){
                    URL = data.urldocs;
                    initializePageObjects(browser,function(){
                    browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                    browser.timeoutsImplicitWait(30000);
                    });
                }
            });

           this.Given(/^User opens the salesforce url with http$/,function(){
               var URL;
               browser = this;
               var execEnv = data["TestingEnvironment"];
               if (execEnv.toUpperCase() == "QA") {
                   URL = data.urldocs1;
                   initializePageObjects(browser,function (){
                       browser.maximizeWindow()
                       .deleteCookies()
                       .url(URL);
                       browser.timeoutsImplicitWait(30000);
                   });
                }
           });
          this.Given(/^User opens the salesforce url with sql query$/,function(){
            var URL7;
            browser = this;
            var execEnv = data["TestingEnvironment"];
            if (execEnv.toUpperCase() == "QA") {
                URL7 = data.urldocs7;
                initializePageObjects(browser,function (){
                    browser.maximizeWindow()
                        .deleteCookies()
                        .url(URL7);
                    browser.timeoutsImplicitWait(30000);
                   });
                }
             });
            this.When(/^user enters username and password$/,function () {
                browser = this;
                var execEnv = data["TestingEnvironment"];
                console.log('Test Environment: ' + execEnv);
                if (execEnv.toUpperCase() == "QA") {
                    initializePageObjects(browser,function () {
                    oneforce_home.waitForElementVisible('@login_id',data.wait);
                    oneforce_home.setValue('@login_id',data.user_name);
                    oneforce_home.waitForElementVisible('@password',data.wait);
                    oneforce_home.setValue('@password',data.pass_word);
                    oneforce_home.waitForElementVisible('@login',data.wait);
                    oneforce_home.click('@login');
             });
          }
    })
            this.When(/^user enters username and password1$/,function () {
                browser = this;
                var execEnv = data["TestingEnvironment"];
                console.log('Test Environment: ' + execEnv);
                if (execEnv.toUpperCase() == "QA") {
                    initializePageObjects(browser, function(){
                    oneforce_home.waitForElementVisible('@login_id', data.wait);
                    oneforce_home.setValue('@login_id',data.user_name);
                    oneforce_home.waitForElementVisible('@password',data.wait);
                    oneforce_home.setValue('@password',data.pass_word1);
                    oneforce_home.waitForElementVisible('@login',data.wait);
                    oneforce_home.click('@login');
            });
          }
    })
            this.When(/^user got error message$/,function () {
                browser = this
                var execEnv = data["TestingEnvironment"];
                console.log('Test Environment: ' + execEnv);
                if (execEnv.toUpperCase() == "QA") {
                    initializePageObjects(browser, function () {
                    oneforce_home.waitForElementVisible('@error_message',data.wait);
                    oneforce_home.getText('@error_message', function(actual){
                    console.log(actual.value)
              })
          });
        }
    })

        this.When(/^Verify that page does not get load$/,function () {
            browser = this
            var execEnv = data["TestingEnvironment"];
            console.log('Test Environment: ' + execEnv);
            if (execEnv.toUpperCase() == "QA") {
                initializePageObjects(browser, function () {
                    oneforce_home.waitForElementVisible('@error_message1',data.wait);
                    oneforce_home.getText('@error_message1', function(actual){
                        console.log(actual.value)
                    })
                });
            }
        })


    this.When(/^user verify dashboard page$/,function (callback) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                browser.pause(5000)
                oneforce_home.waitForElementVisible('@user_menu',data.wait);
                oneforce_home.click('@user_menu');
                oneforce_home.waitForElementVisible('@log_out',data.wait);
                oneforce_home.click('@log_out');
                callback();
            });
        }
    })

    this.When(/^user clicks on logout$/,function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser,function () {
                browser.pause(5000)
                oneforce_home.waitForElementVisible('@user_menu',data.wait);
                oneforce_home.click('@user_menu');
                oneforce_home.waitForElementVisible('@log_out',data.wait);
                oneforce_home.click('@log_out');
            });
        }
    })

    this.Given(/^User identifies AFBO url and uses it in a code snippet$/,function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            URL = data.urldocs2;
            initializePageObjects(browser,function () {
                browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
                browser.timeoutsImplicitWait(30000);
                 });
             }
       });

    this.Then(/^User opens the code snippet from the desktop$/,function ()
    {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        this.pause(3000)
                oneforce_home.waitForElementNotPresent('@login_id', 3000,function(Result) {
    if (Result.value)
    {
        console.log("Not able to access app from iframe- OneForce site is not vulnerable");
    }
    else
    {
        console.log("Able to access app from iframe - OneForce site is vulnerable");
    }
});
});
}